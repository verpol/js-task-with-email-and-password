function validate(form) {
	let userEmail = form.email.value;
	let userPassword = form.password.value;
	let infoSuccess = document.getElementById("success");
	let infoPasswordFail = document.getElementById("passwordFail");
	let infoFail = document.getElementById("fail");

	const ARR = [
		{email: "one@mail.ru", password: "one"},
		{email: "two@mail.ru", password: "two"},
		{email: "three@mail.ru", password: "three"},
		{email: "four@mail.ru", password: "four"},
		{email: "five@mail.ru", password: "five"}
	];

	function check( {email, password} )  {
		let x = 0;
		return new Promise ((resolve, reject) => {
			for (let i = 0; i < ARR.length; i++){
				if (ARR[i].email !== `${email}`) {
					x = 3;
				} else if (ARR[i].password !== `${password}`) {
					x = 2;
					break;
				} else {
					x = 1;
					break;
				}
			}
			switch (x) {
				case 1 : resolve ("User successfully logged in");
					break;
				case 2: reject ({ message: "Password is incorrect for e-mail", code: 1 });
					break;
				case 3: reject ({ message: "User with e-mail does not exist", code: 2 });
					break;
			}
		})
	}

	check({email: `${userEmail}`, password: `${userPassword}`}).then((res) => {
		infoSuccess.style.display="block";
		form.style.display="none";
	}).catch((res) => {
		if (res.code === 1) {
			infoPasswordFail.innerText="Password is incorrect for e-mail " + userEmail;
			infoPasswordFail.style.display="block";
			form.style.display="none";
		} else if (res.code === 2) {
			infoFail.innerText="User with e-mail " + userEmail + " does not exist";
			infoFail.style.display="block";
			form.style.display="none";
		}
	});
}